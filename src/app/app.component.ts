import { Component } from '@angular/core';

@Component({
  selector: 'autovent-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'autovent';
}
