import { browser, by, element } from 'protractor';

export class AutoventPage {
  navigateTo() {
    return browser.get('/');
  }

  getParagraphText() {
    return element(by.css('autovent-root h1')).getText();
  }
}
