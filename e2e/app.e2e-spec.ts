import { AutoventPage } from './app.po';

describe('autovent App', () => {
  let page: AutoventPage;

  beforeEach(() => {
    page = new AutoventPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to autovent!');
  });
});
